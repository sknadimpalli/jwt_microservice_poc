package com.taxilla.employemodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployemoduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployemoduleApplication.class, args);
	}

}

package com.taxilla.employemodule.service;

import com.taxilla.employemodule.entity.Employee;
import com.taxilla.employemodule.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmployeeService {

    @Autowired
    private EmployeeRepository empRepository;


    public Employee saveEmployeeDetails(Employee employee) {
        return empRepository.save(employee);
    }

    public Employee getEmployeeDetailsByUserName(String userName) {
        return empRepository.getEmployeeByUserName(userName);
    }
}

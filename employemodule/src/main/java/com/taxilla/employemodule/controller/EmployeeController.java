package com.taxilla.employemodule.controller;

import com.taxilla.employemodule.entity.Employee;
import com.taxilla.employemodule.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/saveEmpDetails")
    public Employee saveEmployeeDetails( Employee employee, Model model,  HttpServletResponse response, HttpServletRequest request){
        log.info("Inside saveEmployeeDetails method() :: ",this.getClass().getName()," ### ",employee.toString());
        return employeeService.saveEmployeeDetails(employee);
    }

    @GetMapping("/{userName}")
    public Employee getEmployeeDetailsByUserName(@PathVariable("userName") String userName){
        return employeeService.getEmployeeDetailsByUserName(userName);
    }
    
}

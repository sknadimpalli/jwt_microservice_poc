package com.taxilla.employemodule.repository;

import com.taxilla.employemodule.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee ,Long> {
    Employee getEmployeeByUserName(String userName);
}

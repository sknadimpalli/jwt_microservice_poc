package com.taxilla.loginmodule.controller;

import com.taxilla.loginmodule.vo.Employee;
import com.taxilla.loginmodule.vo.LoginDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@EnableWebMvc
public class LoginController {

    @GetMapping("/")
    public ModelAndView userLoginPage(Model model){
        model.addAttribute("loginDetails", new LoginDetails());
        return new ModelAndView("login");
    }

    @PostMapping("/userLogin")
    public ModelAndView userLogin(Model model, LoginDetails details, HttpServletResponse response, HttpServletRequest request){
        RedirectView redirectView = new RedirectView();
        ModelAndView modelAndView;
        if(StringUtils.isEmpty(details.getUserName()) || StringUtils.isEmpty(details.getPassword())){
            System.out.println("Mandatory Values are not provided..");
            return new ModelAndView("login");
        }else {
            String userName = details.getUserName();
            String password = details.getPassword();
            if(false){
                modelAndView = new ModelAndView("success");
            }else{
                model.addAttribute("errorMessage", "Provided User Name Didn't exist in our DB, Please Register");
                modelAndView = new ModelAndView("home");
            }
            model.addAttribute("username", details.getUserName());
            return modelAndView;
        }
    }

    @RequestMapping("/logout")
    public RedirectView logout(Model model) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/");
        return redirectView;
    }

    @RequestMapping("/empRegistration")
    public ModelAndView employeeRegistration(Model model) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/empRegistration");
        model.addAttribute("emRegDetails", new Employee());
        return new ModelAndView("empReg");
    }

    @RequestMapping("/studentRegistration")
    public ModelAndView studentRegistration(Model model) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/studentRegistration");
        model.addAttribute("studentRegDetails", new Employee());
        return new ModelAndView("studentReg");
    }

}

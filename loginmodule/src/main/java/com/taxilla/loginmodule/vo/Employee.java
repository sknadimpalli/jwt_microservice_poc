package com.taxilla.loginmodule.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Long empId;
    private String firstName;
    private String lastName;
    private String email;
    private String doj;
    private String dob;
    private int workExperiecne;
    private String department;
    private String userName;
    private String password;
}

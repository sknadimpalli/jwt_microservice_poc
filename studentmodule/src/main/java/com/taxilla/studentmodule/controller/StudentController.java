package com.taxilla.studentmodule.controller;

import com.taxilla.studentmodule.entity.Student;
import com.taxilla.studentmodule.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService service;

    @PostMapping("/saveStudentDetails")
    public Student saveStudentDetails(Model model, Student student,HttpServletResponse response, HttpServletRequest request){
        log.info("inside SaveStudent Details method, ",this.getClass().getName());
        return service.saveStudentDetails(student);
    }
}

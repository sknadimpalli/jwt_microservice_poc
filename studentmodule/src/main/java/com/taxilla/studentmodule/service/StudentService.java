package com.taxilla.studentmodule.service;

import com.taxilla.studentmodule.entity.Student;
import com.taxilla.studentmodule.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StudentService {

    @Autowired
    private StudentRepository repository;


    public Student saveStudentDetails(Student student) {
        return repository.save(student);
    }
}

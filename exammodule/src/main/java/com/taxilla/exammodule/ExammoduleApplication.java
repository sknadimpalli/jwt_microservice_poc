package com.taxilla.exammodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExammoduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExammoduleApplication.class, args);
	}

}
